pipeline{
    agent any

    environment{
        branch_name = 'main'
        repo_url = 'https://gitlab.com/salmarefaie29/todoapp'
        docker_username = 'salmarefaie29'
        docker_image = 'todo-app'
        trivy_image = 'aquasec/trivy:latest'
    }

    stages{
        stage('git branch'){
            steps{
                git branch:"${branch_name}", url:"${repo_url}"
            }
        }

        stage('static code analysis'){
            steps{
                dir('./app'){
                    script{
                        def scannerHome = tool 'Sonar'
                        withSonarQubeEnv('Sonar'){
                            sh "${scannerHome}/bin/Sonar-scanner"
                        }
                    }
                }
            }
        }

        stage('test application'){
            steps{
                withCredentials([usernamePassword(credentialsId:'docker', usernameVariable:'USERNAME', passwordVariable:'PASSWORD')]){
                    sh """
                    cd ./app 
                    docker login -u ${USERNAME} -p ${PASSWORD}
                    docker-compose up --build -d
                    def curlStatus = sh(script:"curl -s -o /dev/null -w \"%{http_code}\" http://192.168.49.1:3000/api/todos", returnStatus:true).trim()
                    if (curlStatus != '200' && curlStatus != '201' && curlStatus != '204' && curlStatus != '302' && curlStatus != '304'){
                        currentBuild.result = 'FAILURE'
                        error("app not working")
                    }
                    """
                }

            }
            post{
                always{
                    sh"""
                    cd ./app
                    docker-compose down
                    """
                }
            }
        }

        stage('build image'){
            steps{
                withCredentials([usernamePassword(credentialsId:'docker', usernameVariable:'USERNAME', passwordVariable:'PASSWORD')]){
                    sh """
                    cd ./app 
                    docker login -u ${USERNAME} -p ${PASSWORD}
                    docker build --no-cache . -f Dockerfile -t ${docker_username}/${docker_image}:${env.BUILD_NUMBER}
                    """    
                }

            }
        }

        stage('scan vulnerabilities for docker image'){
            steps{
                withCredentials([usernamePassword(credentialsId:'docker', usernameVariable:'USERNAME', passwordVariable:'PASSWORD')]){
                    sh """
                    docker login -u ${USERNAME} -p ${PASSWORD}
                    docker run --rm -v /var/run/docker.sock:/var/run/docker.sock ${trivy_image} image "${docker_username}/${docker_image}:${env.BUILD_NUMBER}" --ignore-unfixed
                    """
                }

            }
        }

        stage('push docker image to docker registery'){
            steps{
                withCredentials([usernamePassword(credentialsId:'docker', usernameVariable:'USERNAME', passwordVariable:'PASSWORD')]){
                    sh """            
                    docker login -u ${USERNAME} -p ${PASSWORD}
                    docker push "${docker_username}/${docker_image}:${env.BUILD_NUMBER}"
                    """
                }
            }

        }

        stage('change yaml files with new image'){
            steps{
                sh """
                cd ./deployment-app
                sed -i 's+${docker_username}/${docker_image}*+${docker_username}/${docker_image}:${env.BUILD_NUMBER}+g' values.yaml
                git add .
                git commit -m "changed"
                git push origin main
                """
            }
        }
    }
}