# DevOps Project
Set up a CI/CD pipeline using Jenkins, automate deployment with ArgoCD, and ensure comprehensive security measures are implemented throughout the process.


## Tools
`GitLab` Containing two repositories, one for the project as whole and one for the CI/CD pipeline.

`Terraform` Building a simple infrastructure for EKS cluster on AWS.

`Ansible` Configuring and installing the packages on the worker node and bastion host.

`Docker` Building image for nodejs application.

`Trivy` Scaning vulnerabilities for application image.

`jenkins` Configuring a CI/CD pipeline for building, testing, and packaging the nodejs application.

`Sonarqube` Implementing static code analysis for the nodejs application.

`Kubernetes` Deploying Nodejs appication with mongodb database and jenkins.

`Helm` Deploying application, argocd and sonarqube.

`Openshift` Deploying Openshift cluster and installing argocd on it.

`Argocd` Deployng nodejs application and mongodb database and Pulling changes automatically.


## Creating Reporisitirs On Gitlab
- I've worked with two repositories for this project:

  #### 1. todoapp:
    - Contains application code, Dockerfile, Docker Compose, and Helm charts.
    - Used for setting up the CI/CD pipeline.
    - Link: https://gitlab.com/salmarefaie29/todoapp

  #### 2. nodejs_app:
    - Contains the complete project.
    - Link: https://gitlab.com/salmarefaie29/nodejs_task

- Basic Git commands used during project:

  ```bash      
   git add .
   git commit -m "message"
   git push origin main
   git pull origin main
  ```


## Building Infrastructure Using Terraform
- Required infrastructure to make EKS Cluster is vpc with 4 subnets; 2 public subnets and 2 private subnets, internet gateway, nat and bastion host in public subnets and worker node in private subnets. we will connect with EKS cluster using Ec2 bastion host. Thee is a s3 bucket to store state file in it to be more secure. 
  
  <p align="center">
  <img src="https://user-images.githubusercontent.com/76884936/221486336-3dd39189-b5a3-41c1-93ef-261448e484b0.png" />
  </p>

- To Run Infrastructure 

  ```bash      
   cd Terraform
   terraform init
   terraform plan
   terraform apply
  ```
- To Destroy Infrastructure 

  ```bash      
   cd Terraform
   terraform destroy
  ```


## Cofgiguring Machines Using Ansible 
- we need to install aws cli, kubectl and helm on ec2 bastion host to connect with EKS cluster using bastion host machine.
- we will connect with bastion host machine using ssh and transfare key to bastion host machine to connect with node worker through bastion host.
- we need to install and start docker on node worker.
- jenkins k8s files was copied to bastion host.
- make config file in ~/.ssh/config

```bash      
      Host bastion-host
        hostname 34.238.167.172      ## change ip for your bastion host
        user ubuntu
        port 22
        identityfile /home/salma/DevOps/nodejs_task/ansible/kashier-task.pem

 ```
 - To run ansible code to machines
 
 ```bash
    cd ansible
    ansible-galaxy init roles/docker
    ansible-galaxy init roles/aws-cli
    ansible-galaxy init roles/kubectl
    ansible-playbook playbook.yaml -i inventory.txt
 ```
 ![cluster1](https://github.com/salmarefaie/ITI-Final-Task-DevOps/assets/76884936/d8f83e6e-80ef-4beb-84ab-926e420df12a)

 
 
 ## Connecting to EKS cluster
 - To connect with EKS using ec2 machine, we will configure aws and then connect to the cluster.
 
 ```bash
    aws configure
    aws eks --region us-east-1 update-kubeconfig --name cluster
    kubectl get nodes
    kubectl get services
 ```

## Deploying Jenkins on EKS Cluster
 - we need to deploy jenkins to configure a CI pipeline for the project. we can install it locally or on any cloud. I made both becauce I need public ip for jenkins to ensure the pipeline is triggered automatically on each commit to the main branch.

```bash
    cd jenkins-k8s
    kubectl apply -f jenkins-deployment.yaml
    kubectl get all -n jenkins
    kubectl exec -it pod/jenkins-dbf6dbcd5-hsqmj -n jenkins /bin/bash   ## to get password for first time
  ```
  ![cluster2](https://github.com/salmarefaie/ITI-Final-Task-DevOps/assets/76884936/e0ea2019-ddee-4e29-a322-94e8ec48173d)
  ![jenkins](https://github.com/salmarefaie/ITI-Final-Task-DevOps/assets/76884936/b0d029c9-75a8-4017-8f7f-fd4ad23cd27b)
  ![jenkins1](https://github.com/salmarefaie/ITI-Final-Task-DevOps/assets/76884936/d7f32b20-300e-490b-872d-e1ac3ba71a7c)
  ![jenkins2](https://github.com/salmarefaie/ITI-Final-Task-DevOps/assets/76884936/23c57f04-0a88-47ad-b235-2f5f51886063)
  ![jenkins3](https://github.com/salmarefaie/ITI-Final-Task-DevOps/assets/76884936/c948b046-b994-4aa9-9d70-b903dfed94b6)


## Building Docker Image
 - The application is a simple todo app using Node.js and MongoDB.
 - Dockerfile was created to build an image for the Node.js app, then it was pushed to Docker Hub.
 - Docker Compose to ensure seamless operation of the Node.js app and MongoDB within a shared network.
 - Mongo database was created and a backup was taken to use it with any mongo container. the backup persists in backup directory.

 ```bash
    cd todoapp
    docker build --no-cache -t salmarefaie29/app-node .  
    docker push salmarefaie29/app-node
  ```
 ![docker1](https://github.com/salmarefaie/ITI-Final-Task-DevOps/assets/76884936/bb417c22-007a-40ee-a1f9-00d220226d88)
 ![docker2](https://github.com/salmarefaie/ITI-Final-Task-DevOps/assets/76884936/346fc342-3efd-4d2e-b2a9-54ca97632448)

  ```bash
     docker exec -it mongodb mongosh
     db.todos.insertOne({ title: "work", description: "finish my tasks" });
     docker exec -it mongodb mongodump --db todo-app --collection todos --out /backup
     docker cp mongodb:/backup ../backup

     docker-compose up --build -d
  ```
  ![docker3](https://github.com/salmarefaie/ITI-Final-Task-DevOps/assets/76884936/c380745d-54d7-4db6-875c-375dbdd81430)


## Deploying Sonarqube on minikube using Helm
 - Sonarqube is used to implement static code analysis for the nodejs application.

```bash
     helm repo add sonarqube https://SonarSource.github.io/helm-chart-sonarqube
     helm repo update
     kubectl create namespace sonarqube
     helm pull sonarqube/sonarqube --untar
     cd sonarqube
     ## I got error that elasticsearch and sonarqube are stopped and I needed to increase max_map_count for vm before installing sonarqube chart.
     sudo sysctl -w vm.max_map_count=262144 
     vi values.yaml         ## change service to LoadBalancer, persistance to true and volume size from 20g to 1g
     helm install sonarqube ../sonarqube/ -n sonarqube
  ```
  ![sonarqube1](https://github.com/salmarefaie/ITI-Final-Task-DevOps/assets/76884936/6499ebf9-9c29-47d0-a311-109bacacee48)
  ![sonarqube2](https://github.com/salmarefaie/ITI-Final-Task-DevOps/assets/76884936/bcdd8657-e454-4439-afcf-f89d33c8ad70)
  ![sonarqube3](https://github.com/salmarefaie/ITI-Final-Task-DevOps/assets/76884936/5ff530e5-6edb-457c-8e63-2a6fd35b9b51)


## Integrating Sonarqube with Jenkins
 1. Generate token from sonarqube
 2. Install sonarqube scanner plugin in jenkins
 3. Add sonarqube token in jenkins credentials
 4. Add sonarQube server URL in configure system section
 5. Install sonar scanner
 6. Configure <sonar-project.properties> and put it with app
 ```bash
    sonar.projectKey=todo-app
    sonar.projectName=todo-app
    sonar.sources=.
    sonar.sourceEncoding=UTF-8
    sonar.scm.disabled=true
 ```

 ![sonarjenkins](https://github.com/salmarefaie/ITI-Final-Task-DevOps/assets/76884936/5c7c3398-09c8-494f-b693-e3fad7689416)

 ![sonarjenkins1](https://github.com/salmarefaie/ITI-Final-Task-DevOps/assets/76884936/ce1d098a-a9b4-4027-9d4b-2c6dbe794538)

 ![sonarjenkins2](https://github.com/salmarefaie/ITI-Final-Task-DevOps/assets/76884936/d1414c39-f9df-49fe-bb97-34724f393884)



## Configure CI Pipeline Using Jenkins
 1. Install webhook plugin and add webhook in gitlab reporisity.
![webhook](https://github.com/salmarefaie/final-task-gcp/assets/76884936/91ded31d-e541-4380-aab9-799a70a3c7ed)
 2. Checkout the repository.
 3. Perform static code analysis using SonarQube.
 4. Conduct functional testing using Docker Compose, as the application doesn't have unit tests. This involves running the application with Docker Compose to verify its functionality.
 5. Build the Docker image.
 6. Scan the Docker image for vulnerabilities using Trivy.
 7. Push the Docker image to Docker Hub.
 8. Update the YAML files in the GitLab repository to reference the new image.


## Deploy an OpenShift cluster
 - First, I attempted to install a local instance of openshift crc. While the installation completed successfully, my laptop froze upon starting the CRC instance, necessitating a restart.

 - Here are the steps I took:
    1. Download openshift local and the pull secret from the Red Hat OpenShift.

    ![openshift1](https://github.com/salmarefaie/ITI-Final-Task-DevOps/assets/76884936/70719dea-7fb2-4dd6-a3d5-88e450a2f30f)

    2. Decompress crc tool and export the path in bashrc.
      ```bash
       cd ~/Downloads/
       tar xvf crc-linux-amd64.tar.xz 
       mkdir -p ~/local/bin
       mv crc-linux-*-amd64/crc ~/local/bin/
       export PATH=$HOME/local/bin:$PATH
       crc version
       echo 'export PATH=$HOME/local/bin:$PATH' >> ~/.bashrc 
      ```

    3. Setup machine 
    ```bash
       crc config set consent-telemetry 
       crc config set cpus 8
       crc config view
       crc setup
    ```

    4. start openshift cluster 
    ```bash
       crc config set consent-telemetry 
       crc config set cpus 8
       crc config view
       crc setup
    ```

    5. access new cluster
    ```bash
       eval $(crc oc-env)
       oc login -u kubeadmin https://api.crc.testing:6443
    ```
![crc1](https://github.com/salmarefaie/ITI-Final-Task-DevOps/assets/76884936/c0f001bf-ca6e-4f65-91f7-74a5cb7caccc)

  - Next, I attempted to install a Red Hat Openshift Service on AWS (ROSA) cluster but requires more qouta ant it took time that aws accepts the request. so I tried another method to create openshift cluster on aws like rose but we mmake all steps from scartch.
   1. Create domain. my domain is called "openshiftforsalma.online".
   2. Create a hosted zone and set a domain name using route 53. 
   3. Crete infrastructure for ec2 and ec2 have at least 2 CPU Core & 2 GiB Memory.
   4. Open redhat to install the requirements. ... create cluster > choose AWS (ARM) > install provisioner infra 
   5. Download openshift installer using wget on ec2 machine.
   6. install configuration for openshift cluster.
   7. create openshift cluster. 
   8. Deploy argocd using helm or openshift operator.
   ![os1](https://github.com/salmarefaie/final-task-gcp/assets/76884936/080914c8-ec51-4f94-b1f8-2f003775f79e)
   ![os2](https://github.com/salmarefaie/final-task-gcp/assets/76884936/5fa0fc99-9251-43ee-8e55-ba256a357b89)
   ![os3](https://github.com/salmarefaie/final-task-gcp/assets/76884936/ba71ae83-b9ca-4b2b-957f-a53ec9e26635)
   ![os4](https://github.com/salmarefaie/final-task-gcp/assets/76884936/a14942bc-4dba-4a8e-a906-56d6182cda73)
   ![os5](https://github.com/salmarefaie/final-task-gcp/assets/76884936/88c29f10-1885-4dda-a579-fb641605b180)
   ![os6](https://github.com/salmarefaie/final-task-gcp/assets/76884936/f4c78f52-805f-4d58-ba87-37dcad1f8f0c)
   ![os7](https://github.com/salmarefaie/final-task-gcp/assets/76884936/8499b211-e0c7-41e1-927d-56f41b8ad5bc)
   ![os8](https://github.com/salmarefaie/final-task-gcp/assets/76884936/ba054574-30ea-46e3-848f-7a1a53fff0de)
   ![os9](https://github.com/salmarefaie/final-task-gcp/assets/76884936/0d45b908-be8b-4d4d-9908-0c5b87e5e8fd)
   ![os10](https://github.com/salmarefaie/final-task-gcp/assets/76884936/76875307-ff3b-45d8-9453-819587c8657c)
   ![os11](https://github.com/salmarefaie/final-task-gcp/assets/76884936/0ac51c5a-83f2-4172-b69b-60511d4bd26d)
   ![os12](https://github.com/salmarefaie/final-task-gcp/assets/76884936/b8c0172e-ffb8-46a1-9122-33504bbd437a)
   ![os13](https://github.com/salmarefaie/final-task-gcp/assets/76884936/838b83b9-3577-41ba-a981-fc37bb2d5f9b)
  

  - Finally, I installed a Red Hat Openshift Service on AWS (ROSA) cluster.

  - Here are the steps I took:
    1. install rosa, Red Hat OpenShift Service on AWS command line interface.
    2. add rosa to your path.
    ```bash
       mv rosa /usr/local/bin/rosa
    ```
    3. enter rosa command to verify the installation:
     ```bash
       rosa 
    ```
    
    4. log in to your Red Hat account with rosa.
     ```bash
       rosa login
    ```
    
    5. verify that AWS account has the necessary permissions.
    ```bash
       rosa verify permisions
    ``` 
    
    6. verify that AWS account has the necessary quota to deploy an Red Hat OpenShift Service on AWS cluster.
    ```bash
       rosa verify quota --region=us-east-1
    ```
    
    7. verify your Red Hat and AWS credentials are setup correctly. 
    ```bash
       rosa whoami
    ```
    
    8. initialize AWS account. 
     ```bash
       rosa init
    ```

    9. download the latest version of the oc CLI.
    ```bash
       rosa download oc
    ```

    10. verify that the oc CLI is installed correctly
    ```bash
       rosa verify oc
    ```

    11. enable rosa from aws 
    
    ![rosa](https://github.com/salmarefaie/ITI-Final-Task-DevOps/assets/76884936/bf3745ef-aadc-4344-934b-3d5839cd0b44)

    
    12. create your cluster with the default cluster settings
    ```bash
       rosa create cluster --cluster-name=openshift
    ```
    
    13. track the progress of the cluster creation by watching the OpenShift installer logs
    ```bash
       rosa logs install --cluster=openshift --watch
    ```
![oc1](https://github.com/salmarefaie/ITI-Final-Task-DevOps/assets/76884936/147d6c18-8fce-48dd-ad6c-26d0c4e028ee)
![oc2](https://github.com/salmarefaie/ITI-Final-Task-DevOps/assets/76884936/660c7949-5c32-431e-b8cf-bd8df0fad7b2)
![oc3](https://github.com/salmarefaie/ITI-Final-Task-DevOps/assets/76884936/a0150676-dff1-4e5b-8588-cdaf713760d3)
![oc4](https://github.com/salmarefaie/ITI-Final-Task-DevOps/assets/76884936/ddfc091b-4b1a-43e4-9a61-7b1974c4d35d)

    
    14. create admin
    ```bash
        rosa create admin --cluster=openshift
    ```
![o1](https://github.com/salmarefaie/final-task-gcp/assets/76884936/68cb95a2-3f7b-438e-9b67-8fc93a1cbb30)
![o2](https://github.com/salmarefaie/final-task-gcp/assets/76884936/e5bbad71-1011-47ac-8bd1-1e4c87e462bf)
![o3](https://github.com/salmarefaie/final-task-gcp/assets/76884936/5966b63a-287d-4321-b3be-9aaf48fd0ae3)


## Deploy argocd and nodejs on OpenShift cluster
 - Using Argo CD to deploy a Node.js application and pull changes automatically.
 - There are two options for automatic pulls in argocd; configuring automatic pulls in argocd.yaml and this can be done by specifying the appropriate settings in the argocd.yaml file. the other is creating a webhook in the gitLab repository and linking it to argocd and this allows argocd to be notified whenever there are changes in the repository and trigger a deployment automatically. However, that requires a public IP address for argocd to be accessible by the webhook.
 - I have already prepared the deployment files for the application using helm charts and kubernetes yaml files and I tried it on minikube also.
 - I configured argocd.yaml to point argocd to the gitlab repository where the application's deployment YAML files reside. 

![app](https://github.com/salmarefaie/ITI-Final-Task-DevOps/assets/76884936/40cd7c14-c7b6-4681-be2f-538390ecad32)

![app1](https://github.com/salmarefaie/ITI-Final-Task-DevOps/assets/76884936/451a25a8-4760-4ab8-af2b-c32b0b7bd85d)

![argoo](https://github.com/salmarefaie/ITI-Final-Task-DevOps/assets/76884936/402db2b6-6c11-4e68-ab96-d785cf98ede4)

![argo](https://github.com/salmarefaie/ITI-Final-Task-DevOps/assets/76884936/63e580be-2fd8-4688-a9f9-d5f16d630295)

![argocd](https://github.com/salmarefaie/ITI-Final-Task-DevOps/assets/76884936/72c7febc-5eab-493b-9c81-25412903edd7)

![argo1](https://github.com/salmarefaie/ITI-Final-Task-DevOps/assets/76884936/185b77d0-83c0-41d0-993d-ef57bdbb2065)

- To Deploy argocd using helm on openshift cluster
```bash
    oc create namespace argocd
    helm repo add argo-cd https://argoproj.github.io/argo-helm
    helm install argocd argo-cd/argo-cd -n argocd
    oc expose svc argocd-server -n argocd
    oc get route argocd-server -n argocd
    oc port-forward svc/argocd-server -n argocd 8080:443
    oc get secret argocd-initial-admin-secret -n argocd -o yaml   ## get password for argocd
    echo <passwor>| base64 --decode
``` 

- To Deploy application using argocd
```bash
    kubectl apply -f argocd.yaml
``` 

- To Deploy application using k8s on minikube
```bash
    cd todoapp-k8s
    kubectl apply -f todoapp.yaml
    kubectl exec -it mongodb-0 -n todoapp sh 
    mongorestore --drop /backup/backup/todo-app/todos.bson 
    exit
``` 

- To Deploy application using k8s on minikube
```bash
    helm create todoapp-helm
    helm install todoapp todoapp-helm/ --values todoapp-helm/values.yaml -n todoapp
``` 

- To Deploy argocd using helm on minikube
```bash
    git clone https://github.com/argoproj/argo-helm.git
    cd argo-helm/charts/argo-cd/
    kubectl create ns argocd
    helm dependency up
    helm install argocd . -f values.yaml -n argocd
``` 


## To run project on your local machine
 1. Clone the reporisity
    ```bash
      git clone git@gitlab.com:salmarefaie29/nodejs_task.git
    ```

 2. Add the app and deployment files on another reporisity.
    ```bash
      https://gitlab.com/salmarefaie29/todoapp
    ```
 
 3. Run Infrastructure as code to create infrastructure on Aws.
    ```bash      
      cd Terraform
      terraform init
      terraform plan
      terraform apply
    ```

 4. Configure worker node and bastion host machines using ansible.
    ```bash
       cd ansible
       ansible-playbook playbook.yaml -i inventory.txt
     ```

 5. Connect to EKS cluster using bastion host.
    ```bash
       aws configure
       aws eks --region us-east-1 update-kubeconfig --name cluster
       kubectl get services
    ```

 6. Deploy Jenkins on EKS Cluster
    ```bash
      cd jenkins-k8s
      kubectl apply -f jenkins-deployment.yaml
      kubectl get all -n jenkins
      kubectl exec -it pod/jenkins-dbf6dbcd5-hsqmj -n jenkins /bin/bash   ## to get password for first time
    ```

 7. Deploy Sonarqube on minikube using Helm.
    ```bash
     kubectl create namespace sonarqube
     cd sonarqube
     sudo sysctl -w vm.max_map_count=262144 
     helm install sonarqube ../sonarqube/ -n sonarqube  # user:admin && # first password: admin 
    ```

  8. Integrate Sonarqube with Jenkins  
   - Generate token from sonarqube ... My Account > Security > Generate token
   - Install sonarqube scanner plugin in jenkins  .. Manage Jenkins > Plugin Manager > Install Sonarqube Scanner Plugin
   - Add sonarqube token in jenkins credentials  ... Manage Jenkins > Manage Credentials > global > Add Credentials
   - Add sonarQube server URL ... Manage Jenkins > Configure System > SonarQube servers
   - Install sonar scanner ... Manage Jenkins > Global Tool Configuration


  9. Configure CI Pipeline
   - Add docker credentials in n jenkins credentials .. Manage Jenkins > Manage Credentials > global > Add Credentials.
   - Add git credentials if your repo is private. my repo is public, i didn't need to do that.
   - Install webhook plugin. ... Manage Jenkins > Plugin Manager > webhook trigger Plugin
   - Add webhook in gitlab reporisity. ... <your_repo> > setting > webhook > add new webhook 
   - url in webhook should be <jenkins_url>/generic-webhook-trigger/invoke?token=<your_token>
   - Create pipeline , add new item > pipeline, choose generic webhook trigger in build trigger and configure pipeline.
   - Copy Jenkinsfile in pipeline script or choose pipeline script from scm and put Jenkinsfile path.


  10. Install a Red Hat Openshift Service on AWS (ROSA) cluster.
    - install rosa, Red Hat OpenShift Service on AWS command line interface.
    - enable rosa from aws 
      ```bash
       mv rosa /usr/local/bin/rosa
       rosa
       rosa login
       rosa verify permisions
       rosa verify quota --region=us-east-1
       rosa whoami
       rosa init
       rosa download oc
       rosa verify oc
       rosa create cluster --cluster-name=openshift
       rosa logs install --cluster=openshift --watch
      ```
   
   11. Deploy argocd on opensghift cluster.
       ```bash
       oc create namespace argocd
       helm repo add argo-cd https://argoproj.github.io/argo-helm
       helm install argocd argo-cd/argo-cd -n argocd
       oc expose svc argocd-server -n argocd
       oc get route argocd-server -n argocd
       oc port-forward svc/argocd-server -n argocd 8080:443
       oc get secret argocd-initial-admin-secret -n argocd -o yaml   ## get password for argocd
       echo <passwor>| base64 --decode
       ``` 

   12. Deploy app using argocd
       ```bash
       oc apply -f argocd.yaml
       ``` 
